import pandas as pd


def get_data(path: str):
    data = pd.read_csv(path)

    return data
